# ACAP
This is the landing page for the implementations that are used for the work _Exploring the Versal AI Engines for Signal Processing in Radio Astronomy_.

## This Project
Nowadays, heterogeneous architectures are widely used to overcome the ongoing demand for increased computing performance at the edge, such as the pre-processing of raw antenna data in radio telescope systems. The Versal Adaptive SoC is a novel heterogeneous architecture that includes Programmable Logic (PL), a Processing System, and AI Engines interconnected by a programmable Network-on-Chip. In this work, we explore the AI Engines to evaluate their capabilities for real-time signal processing in radio telescope systems. We focus on the implementation of a Polyphase Filter Bank (PFB), which is a representative signal processing operation that consists of Finite Impulse Response (FIR) filters and the Fast-Fourier Transform (FFT) algorithm. We analyzed the performance of the AI Engines with regard to the requirements of LOFAR, the world's largest low-frequency radio telescope. By means of the roofline model, we reveal that the AI Engines theoretically meet the LOFAR computational requirements, but the FIR vendor-provided library implementation did not reach the required performance. Therefore, we explore several optimization strategies for the FIR implementation on the AI Engines and analyze the communication options between the PL and the AI Engine array. Finally, we have developed an efficient PFB implementation that requires only 12 AI Engines. A prototype on a VC1902 device achieves a throughput of 437 MSPS, which is more than sufficient for a single antenna polarization of the LOFAR system.


## Publication
If you use this work, please cite the following publication:
Victor van Wijhe, Vincent Sprave, Daniele Passaretti, Nikolaos Alachiotis, Gerrit Grutzeck, Thilo Pionteck, and
Steven van der Vlugt. 2024. Exploring the Versal AI Engines for Signal Processing in Radio Astronomy. In 2024 34rd
International Conference on Field-Programmable Logic and Applications (FPL).

## Where to find what
- [FIR, FFT and PFB AI Engine implementations used in this work](https://git.astron.nl/RD/recruit/acap-ppf)
- [PFB reference implementation, adapted to fit with the AI Engine work](https://github.com/svlugt/pfb_introduction)
- [Implementation of the prototype on the VCK190 development kit](https://github.com/HTI-OVGU/ACAP-HTI)
- [Essay (Master): SIGNAL PROCESSING WITH AMD ADAPTIVE COMPUTE ACCELERATION PLATFORM (ACAP) FOR APPLICATIONS IN RADIO ASTRONOMY](https://essay.utwente.nl/98354/)

## Authors 
- Victor van Wijhe; 1,3
- Vincent Sprave; 2
- Daniele Passaretti; 2
- Nikolaos Alachiotis; 3
- Gerrit Grutzeck; 4
- Thilo Pionteck; 2
- Steven van der Vlugt; 1

Affiliations:
1. Netherlands Institute for Radio Astronomy (ASTRON), 7990 AA Dwingeloo, The Netherlands
2. Otto-von-Guericke University Magdeburg, 39106 Magdeburg, Germany
3. Faculty of EEMCS, University of Twente, The Netherlands
4. Max Planck Institute for Radio Astronomy, 53121 Bonn, Germany

## Acknowledgment
This project has received funding from the European Union's Horizon Europe research and innovation program under grant agreement no. 101093934, from the Netherlands eScience Center through the RECRUIT project and has made use of resources and expertise provided by SURF Experimental Technologies Platform (SURF–ETP), which is part of the SURF cooperative in the Netherlands, under project no. SURF–ETP-0006. We thank Jan Timmer and Roel Jordans from TU Eindhoven for a preliminary analysis of the AI Engines performance. We thank our other colleagues and partners for valuable feedback on the projects through a.o. a [workshop, hosted at ASTRON](https://www.astron.nl/dailyimage/main.php?date=20230727).

## License
This project comes with an Apache 2.0 license